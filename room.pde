/**
*Clase para crear tiles e indicarle si son o no obstaculos
*Tambien agregarle un color
*/
class Tile
{
	int x;
  	int y;

	color col;
	boolean obstacle;

  	Tile(int x, int y)
  	{
	    this.x = x;
	    this.y = y;

		//definimos las paredes y su color
		if (this.x == 0 || this.x == Room.TILES_ON_X - 1 ||
      	this.y == 0 || this.y == Room.TILES_ON_Y - 1)
	    {
	      setObstacle(true);
	    }
	    else
	      setObstacle(false);
	}

	//funcion que le dira a un tile si es o no un obstaculo
	void setObstacle(boolean state)
  	{
	    this.obstacle = state;
	    if (this.obstacle)
	      this.col = color(128);
	    else
	    {
			//pintamos los tiles para distinguirlos segun su posicion.
			if ((this.x + this.y) % 2 == 0)
	        	this.col = color(32);
	      	else
	        	this.col = color(40);
	    }
  	}
};

/**
*Clase para crear el cuarto de inicio de juego, extiende de la clase Room
*/
class RoomStart extends Room
{

};

/**
*Clase para crear el cuarto de salida de juego, extiende de la clase Room
*/
class RoomEnd extends Room
{
	
};

/**
*Clase para la sala donde se puede jugar, esta contiene Tile
*/
class Room
{
	//Cantidad de Tiles en el eje x, y
	static final int TILES_ON_X = 16;
	static final int TILES_ON_Y = 12;

	//Variables x e y que representan la coordenada del room dentro del mapa
	int x;
	int y;

	Tile[][] tiles;
	DMap dmap;

	//variable que nos indicaran en donde estan las puertas.
	boolean doorLeft;
	boolean doorRight;
	boolean doorTop;
	boolean doorBottom;

	Room()
	{
		//Inicializamos e indicamos cuantos tiles va a tener en x e y, la sala.
		this.tiles = new Tile[Room.TILES_ON_X][Room.TILES_ON_Y];

		//recorremos todos los tiles
		for (int y = 0; y < Room.TILES_ON_Y; y++)
      		for (int x = 0; x < Room.TILES_ON_X; x++)
        		this.tiles[x][y] = new Tile(x, y); //le pasamos e indicamos donde se va a crear el nuevo tile
	}

	void createDMap()
	{
		this.dmap = new DMap(Room.TILES_ON_X, Room.TILES_ON_Y);
		for (int y = 0; y < Room.TILES_ON_Y; y++)
      		for (int x = 0; x < Room.TILES_ON_X; x++)
			{
				Tile t = this.tiles[x][y];
				if (t != null && t.obstacle)
					this.dmap.setObstacle(x, y);
				
			}
	}

	//Funcion encargada de generar las puertas si existen las conecciones entre los dungeons
	void openDoors()
	{
		if (this.doorLeft)
		{
			this.tiles[0][Room.TILES_ON_Y / 2 - 1].setObstacle(false);
			this.tiles[0][Room.TILES_ON_Y / 2].setObstacle(false);
		}
		if (this.doorRight)
		{
			this.tiles[Room.TILES_ON_X - 1][Room.TILES_ON_Y / 2 - 1].setObstacle(false);
			this.tiles[Room.TILES_ON_X - 1][Room.TILES_ON_Y / 2].setObstacle(false);
		}
		if (this.doorTop)
		{
			this.tiles[Room.TILES_ON_X / 2 - 1][0].setObstacle(false);
			this.tiles[Room.TILES_ON_X / 2][0].setObstacle(false);
		}
		if (this.doorBottom)
		{
			this.tiles[Room.TILES_ON_X / 2 - 1][Room.TILES_ON_Y - 1].setObstacle(false);
			this.tiles[Room.TILES_ON_X / 2][Room.TILES_ON_Y - 1].setObstacle(false);
		}
	}

	//funcion que dice a partir de un valor en px en que Tile esta parado el player en el eje X
	int getTileX(float x)
  	{
	    float tileW = width / float(Room.TILES_ON_X);
	    int tx = int(floor(x / tileW));
	    if (tx < 0)
	      tx = 0;
	    else if (tx >= Room.TILES_ON_X)
	      tx = Room.TILES_ON_X - 1;
	    return tx;
  	}

	//funcion que dice a partir de un valor en px en que Tile esta parado el player en el eje Y
	int getTileY(float y)
  	{
	    float tileH = height / float(Room.TILES_ON_Y);
	    int ty = int(floor(y / tileH));
	    if (ty < 0)
	      ty = 0;
	    else if (ty >= Room.TILES_ON_Y)
	      ty = Room.TILES_ON_Y - 1;
	    return ty;
  	}

	//funcion que devuelve los tiles contra los que esta chocando el player
	ArrayList<Tile> getOverlappingTiles(RoomObject robj)
  	{
	    ArrayList<Tile> overlapping = new ArrayList<Tile>();

	    float radius = width / robj.getRadius();

	    float left = player.pos.x - radius * 0.5f;
	    float right = player.pos.x + radius * 0.5f;
	    float top = player.pos.y - (radius * 0.25f) * 0.5f;
	    float bottom = player.pos.y + (radius * 0.25f) * 0.5f;

		//obtenemos los valores de los tile donde esta parado ej '(x = 1, y = 0)'
		int leftTile = getTileX(left);
	    int rightTile = getTileX(right);
	    int topTile = getTileY(top);
	    int bottomTile = getTileY(bottom);

		//recorremos los tiles desde el de arriba hasta el de abajo inclusive en x e y
		for (int y = topTile; y <= bottomTile; y++)
	      for (int x = leftTile; x <= rightTile; x++)
	        overlapping.add(this.tiles[x][y]); //agregamos a la lista de overlapping a los tiles 


		//retornamos los tiles contra los que esta "colisionando"
		return overlapping;
	}

	void draw()
	{
		//indicamos cuanto mide un tile
		float tileW = width / float(Room.TILES_ON_X);
    	float tileH = height / float(Room.TILES_ON_Y);

		noStroke();
		//recorremos todos los tiles
		for(int y = 0; y < Room.TILES_ON_Y; y++)
			for(int x = 0; x < Room.TILES_ON_X; x++)
			{
				//agregamos un color al tile
				fill(this.tiles[x][y].col);
				//le indicamos donde se van dibujando los tiles
				rect(x * tileW, y * tileH, tileW, tileH);
			}
	}
};