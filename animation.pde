class AnimationManager
{
    //HashMap es lo mismo que un array, pero para un valor se le guarda un valor... son como cajones con nombres.
    HashMap<String, Animation> animations;
    Animation activeAnimation;
    String activeAnimationID;

    AnimationManager()
    {
        this.animations = new HashMap<String, Animation>();
        this.activeAnimation = null;
        this.activeAnimationID = null;
    }

    //funcion encargada de agregar las animacion, si la animacion no existe la crea
    void addFrame(String animID, PImage frame)
    {
        Animation anim = this.animations.get(animID);
        //pregunta si no existe la animacion
        if (anim == null)
        {
            anim = new Animation();
            this.animations.put(animID, anim);
        }
        //agregamos el frame
        anim.addFrame(frame);
    }

    //elegimos que animacion de las que tenemos en esta clase elegimos que este activa
    void setActive(String animID)
    {
        //checkeamos que la animacion activa no sea nula y si ya estamos haciendo la animacion que querememos reproducir y no hacemos ningun cambio
        if (this.activeAnimationID != null && this.activeAnimationID.equals(animID))
            return;
        
        this.activeAnimation = this.animations.get(animID);
        this.activeAnimationID = animID;
        this.activeAnimation.reset();
    }

    void draw(float dt)
    {
        //agrega la animacion actual y la dibuja
        this.activeAnimation.draw(dt);
    }

};

//Clase encargada de realizar las animaciones del juego
class Animation
{
    final static float FRAMES_PER_SECOND = 6.0f;
    final static float FRAME_DURATION = 1.0f / FRAMES_PER_SECOND;

    ArrayList<PImage> frames;
    int currentFrame;
    float frameTime;

    Animation()
    {
        this.frames = new ArrayList<PImage>();
        //reseteamos el currentFrame y el setTime
        reset();
    }

    //reseteamos la animacion... currentFrame a 0 para que la animacion no arranque poor la mitad y que el frameTime sea 0 para que la animacion no dure algo que no deva durar
    void reset()
    {
        this.currentFrame = 0;
        this.frameTime = 0;
    }

    //funcion que permitira agregar un frames
    void addFrame(PImage frame)
    {
        this.frames.add(frame);
    }

    //dibujamos las animaciones
    void draw(float dt)
    {
        //chequeamos si no hay frames en la animacion no hacemos nada
        if (this.frames.size() == 0)
            return;

        //iniciamos con la imagen de idle
        image(this.frames.get(this.currentFrame), 0, 0);
        //le sumamos al frameTime el delta time para saber la cantidad de tiempo q llevamos en un frame
        this.frameTime += dt;

        //mientras que el frameTime sea mayor que la duracion de un frame le restamos una duracion entera y hacemos que el frame actual avanze
        while (this.frameTime > FRAME_DURATION)
        {
            this.frameTime -= FRAME_DURATION;
            this.currentFrame = (this.currentFrame + 1) % this.frames.size();
        }
    }
};