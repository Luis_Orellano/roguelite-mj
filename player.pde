class Player extends RoomObject
{
	AnimationManager anims;
	Weapon weapon;

	Player()
	{
		//inicializamos los Vectores.
		super(new PVector(width/2, height/2));
		this.weapon = new Weapon(true, 4);
		//cargamos el sheet con las animaciones
		SpriteSheet ssheet;
		ssheet = new SpriteSheet("animation_sheet.png", 17, 20, 1, 1);

		this.anims = new AnimationManager();
		this.anims.addFrame("idle", ssheet.frames[0]);
		this.anims.addFrame("walk", ssheet.frames[1]);
		this.anims.addFrame("walk", ssheet.frames[2]);
		this.anims.setActive("idle");
	}

	float getRadius()
	{
		return 20.0f;
	}

	void update()
	{
		//agregamos la velocidad necesaria cuando se aprieta un boton de movimiento
		final float SPEED = 1.0f;
	    if (kLEFT)
	      this.vel.x -= SPEED;
	    if (kRIGHT)
	      this.vel.x += SPEED;
	    if (kUP)
	      this.vel.y -= SPEED;
	    if (kDOWN)
	      this.vel.y += SPEED;

		//checkeamos si nos estmaos moviendo si la velocidad es mayor a 0.5, sino dejamos la animacion en idle
		if (this.vel.mag() > 0.5f)
			this.anims.setActive("walk");
		else 
			this.anims.setActive("idle");
		
		super.update();

		if (frameCount % 8 == 0)
		{
			currentRoom.dmap.reset();
			int px = currentRoom.getTileX(this.pos.x);
			int py = currentRoom.getTileY(this.pos.y);
			currentRoom.dmap.setGoal(px, py);
			currentRoom.dmap.run();
		}
		
		

		//Si el player pasa por una de las puertas va al siguiente dungeon
		// 
		if (this.pos.x < 0)
		{
			this.pos.x += width;
			dungeon.move(-1, 0);
		}
		if (this.pos.y < 0)
		{
			this.pos.y += height;
			dungeon.move(0, -1);
		}
		if (this.pos.x >= width)
		{
			this.pos.x -= width;
			dungeon.move(1, 0);
		}
		if (this.pos.y >= height)
		{
			this.pos.y -= height;
			dungeon.move(0, 1);
		}

		if (mouseLeft)
		{
			float radius = width / getRadius();

			PVector weaponOrigin = new PVector(this.pos.x, (this.pos.y - radius * 0.65f));
			PVector weaponTarget = new PVector(mouseX, mouseY);
			this.weapon.fire(weaponOrigin, weaponTarget);
		}

		this.weapon.update();
	}

	void draw(float dt)
	{
		float radius = width / getRadius();

		//angulo entre el centro de la sombra del personaje y el mouse
		float angle = atan2(mouseY - (this.pos.y - radius * 0.65f), mouseX - this.pos.x); 
		stroke(255, 0, 0, 192);

		PVector hitPoint = new PVector();
		//llamamos a la funcion encargada del raycast del player
		boolean foundNoObstacle = raycast(int(this.pos.x), int((this.pos.y - radius * 0.65f)), mouseX, mouseY, hitPoint);
		line(this.pos.x, (this.pos.y - radius * 0.65f), hitPoint.x, hitPoint.y);

		this.weapon.draw();
		if (foundNoObstacle == false)
		{
			noStroke();
			fill(255, 0, 0);
			ellipse(hitPoint.x, hitPoint.y, 3, 3);
		}

		//creamos matriz para dibujar y transladamos el eje de coordenadas
		pushMatrix();
			translate(this.pos.x, this.pos.y);
			if(this.pos.x < mouseX)
				scale(-1.0f, 1.0f);

			noStroke();
			//le damos color a la sombra y la dibujamos.
			fill(8);
			ellipse(0, 0, radius, radius * 0.25f);

			//dibujamos el personaje
			translate(0.0f, -radius * 0.65f);
			scale(radius / 17);
			imageMode(CENTER);
    		this.anims.draw(dt);
			imageMode(CORNER);
		//destruimos la matriz
		popMatrix();

	}
};