class RoomObject
{
	PVector pos;
	PVector vel;

	RoomObject(PVector pos)
	{
		//inicializamos los Vectores.
		this.pos = pos;
		this.vel = new PVector();
	}

	float getRadius()
	{
		return 1.0f;
	}

	//devuelve si esta colisionando contra un obstaculo o no
	boolean isColliding()
	{
	    ArrayList<Tile> overlapping = currentRoom.getOverlappingTiles(this);
	    for (Tile t : overlapping)
	    {
	      if (t.obstacle == false)
	        continue;
	      return true;
	    }
	    return false;
	}

	//Checkea y devuelve el mejor tile contra el que colisionar
  	Tile bestTileToCollide()
  	{
	    float tileW = width / float(Room.TILES_ON_X);
	    float tileH = height / float(Room.TILES_ON_Y);
	    ArrayList<Tile> overlapping = currentRoom.getOverlappingTiles(this);

	    float shortestOnX = 999999.0f;
	    Tile bestTileOnX = null;
	    float shortestOnY = 999999.0f;
	    Tile bestTileOnY = null;
	    for (Tile t : overlapping)
	    {
	      if (t.obstacle == false)
	        continue;

	      float tLeft = t.x * tileW;
	      float tCenterX = tLeft + tileW * 0.5f;
	      float tTop = t.y * tileH;
	      float tCenterY = tTop + tileH * 0.5f;

	      float distanceX = abs(this.pos.x - tCenterX);
	      if (shortestOnX > distanceX)
	      {
	        shortestOnX = distanceX;
	        bestTileOnX = t;
	      }
	      float distanceY = abs(this.pos.y - tCenterY);
	      if (shortestOnY > distanceY)
	      {
	        shortestOnY = distanceY;
	        bestTileOnY = t;
	      }
	    }

	    if (shortestOnX < shortestOnY)
	      return bestTileOnX;
	    else
	      return bestTileOnY;
  	}

	//Devuelve si tiene q colisionar primero en X
	boolean collideOnXFirst(Tile t)
  	{
	    float tileW = width / float(Room.TILES_ON_X);
	    float tileH = height / float(Room.TILES_ON_Y);

	    float largestOnX = -999999.0f;
	    float largestOnY = -999999.0f;

	    float tLeft = t.x * tileW;
	    float tCenterX = tLeft + tileW * 0.5f;
	    float tTop = t.y * tileH;
	    float tCenterY = tTop + tileH * 0.5f;

	    float distanceX = abs(this.pos.x - tCenterX);
	    if (largestOnX < distanceX)
	      largestOnX = distanceX;
	    float distanceY = abs(this.pos.y - tCenterY);
	    if (largestOnY < distanceY)
	      largestOnY = distanceY;

	    return largestOnX > largestOnY;
  	}

	//devuelve true o false dependiendo si colisiono o no en el eje X con un TILE
	void collideXWithTile(Tile t)
  	{
    	float radius = width / getRadius();
		//indicamos cuanto mide un tile en X
		float tileW = width / float(Room.TILES_ON_X);

		//Deteccion de la colision en X
		float tLeft = t.x * tileW;
	    float tCenterX = tLeft + tileW * 0.5f;
	    if (this.pos.x < tCenterX)
	    {
	      this.pos.x = tLeft - radius * 0.5f - 0.025f;
	      this.vel.x = 0.0f;
	    }
	    else
	    {
	      this.pos.x = tLeft + tileW + radius * 0.5f + 0.025f;
	      this.vel.x = 0.0f;
	    }
		
	}

	//devuelve true o false dependiendo si colisiono o no en el eje Y con un TILE
	void collideYWithTile(Tile t)
  	{
    	float radius = width / getRadius();
		//indicamos cuanto mide un tile en Y
		float tileH = height / float(Room.TILES_ON_Y);

		//Deteccion de la colision en Y
		float tTop = t.y * tileH;
	    float tCenterY = tTop + tileH * 0.5f;
	    if (this.pos.y < tCenterY)
	    {
	      this.pos.y = tTop - (radius * 0.25f) * 0.5f - 0.025f;
	      this.vel.y = 0.0f;
	    }
	    else
	    {
	      this.pos.y = tTop + tileH + (radius * 0.25f) * 0.5f + 0.025f;
	      this.vel.y = 0.0f;
	    }
		
	}

	//funcion para checkear si existen colisiones
	void collideWithRoom()
  	{
	    while (isColliding())
	    {
	      Tile t = bestTileToCollide();
	      if (collideOnXFirst(t))
	        collideXWithTile(t);
	      else
	        collideYWithTile(t);
	    }
  	}

	void update()
	{
		//le agregamos friccion al player
		this.vel.mult(0.9f);
	    if (this.vel.mag() < 0.1f)
	      this.vel.set(0.0f, 0.0f);
		else //le damos un limite de velocidad al player
			this.vel.limit(5.0f);

		collideWithRoom();

		//agregamos la velocidad al vector
		this.pos.add(this.vel);
	}
};