class Weapon
{
    boolean instant;
    int framesFiring;

    boolean firing;
    int framesSiceFire;
    PVector lastOrigin;
    PVector lastHitPoint;

    Weapon(boolean instant, int framesFiring)
    {
        this.instant = instant;
        this.framesFiring = framesFiring;
    }

    void hit(PVector hiPoint)
    {
    }

    void missed(PVector hitPoint)
    {
    }

    void fire(PVector origin, PVector target)
    {
        
        if (this.firing)
            return;

        this.framesSiceFire = 0;
        this.firing = true;
        if (this.instant)
        {
            PVector hitPoint = new PVector();
            boolean foundNoObstacle = raycast(int(origin.x), int(origin.y), int(target.x), int(target.y), hitPoint);

            if(foundNoObstacle)
                hit(hitPoint);
            else 
                missed(hitPoint);
            
            this.lastHitPoint = hitPoint.copy();
            this.lastOrigin = origin.copy();
        }
    }
    
    void update()
    {
        this.framesSiceFire++;
        if (this.framesSiceFire > this.framesFiring)
            this.firing = false;
    }

    void draw()
    {
        if (this.firing)
        {
            float t = this.framesSiceFire / float(this.framesFiring);
            strokeWeight(t * 4.0f);
            stroke(255, 0, 0);
            line(this.lastOrigin.x, this.lastOrigin.y, this.lastHitPoint.x, this.lastHitPoint.y);
        }
        strokeWeight(1.0f);
    }
};