Dungeon dungeon;
Room currentRoom;
Player player;
Enemy enemy;

int lastDraw;

void setup()
{
  size(1024, 768);
  noSmooth();

  dungeon = new Dungeon(8, 8, 8, 14);
  currentRoom = dungeon.startRoom();
  currentRoom.tiles[4][1].setObstacle(true);
  currentRoom.tiles[4][2].setObstacle(true);
  currentRoom.tiles[4][3].setObstacle(true);
  currentRoom.tiles[4][4].setObstacle(true);
  currentRoom.tiles[5][4].setObstacle(true);
  currentRoom.tiles[6][4].setObstacle(true);

  currentRoom.tiles[6][6].setObstacle(true);
  currentRoom.tiles[6][7].setObstacle(true);
  currentRoom.tiles[7][7].setObstacle(true);
  currentRoom.tiles[8][7].setObstacle(true);
  currentRoom.tiles[9][7].setObstacle(true);
  currentRoom.tiles[9][8].setObstacle(true);
  currentRoom.tiles[9][9].setObstacle(true);
  currentRoom.createDMap();

  player = new Player();
  enemy = new Enemy(new PVector(width * 0.75f, height * 0.5f));

  lastDraw = millis();
}

void draw()
{
  //calculamos cuanto tiempo paso desde que se hizo un draw y lo guardamos, esto nos va a dar cuantos milisegundo pasaron
  int now = millis();
  int frameTime = now - lastDraw;
  lastDraw = now;
  //guardamos un delta time para dibujar las animaciones
  float dt = frameTime / 1000.0f;


  player.update();
  enemy.update(dt);

  background(255, 0, 255);
  currentRoom.draw();

  enemy.draw(dt);
  player.draw(dt);
  
  dungeon.drawMap(0, 0, 160, 120);
}

boolean kLEFT = false;
boolean kRIGHT = false;
boolean kUP = false;
boolean kDOWN = false;

void keyPressed()
{
  if (key == 'a') kLEFT = true;
  if (key == 'd') kRIGHT = true;
  if (key == 'w') kUP = true;
  if (key == 's') kDOWN = true;
}

void keyReleased()
{
  if (key == 'a') kLEFT = false;
  if (key == 'd') kRIGHT = false;
  if (key == 'w') kUP = false;
  if (key == 's') kDOWN = false;

  if (key == ' ') dungeon.regenerate(8, 14);
}

boolean mouseLeft = false;
void mousePressed()
{
  if (mouseButton == LEFT)
    mouseLeft = true;
}

void mouseReleased() 
{
  if (mouseButton == LEFT)
    mouseLeft = false;  
}

//tiempo actual video!
//https://youtu.be/4si1NePOcrE?t=4176