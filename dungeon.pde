class Dungeon
{
	int w;
	int h;
	Room[][] rooms;

	Dungeon(int w, int h, int roomAmountMin, int roomAmountMax)
	{
		this.w = w;
		this.h = h;

		regenerate(roomAmountMin, roomAmountMax);
	}

	//Funcion encargada de devolver el cuarto de entrada
	Room startRoom()
	{
		for (int yy = 0; yy < this.h; yy++)
		{
			for (int xx = 0; xx < this.w; xx++)
			{
				//Guardamos el cuarto de entrada y lo devolvemos
				Room r = this.rooms[xx][yy];
				if (r == null)
					continue;
				if (r instanceof RoomStart)
					return r;
			}
		}
		return null;
	}

	//Funcion encargada de mover al player entre los dungeons
	void move(int dx, int dy)
	{
		currentRoom = this.rooms[currentRoom.x + dx][currentRoom.y + dy];
	}

	//Funcion para regenerar el mapa del dungeon
	void regenerate(int roomAmountMin, int roomAmountMax)
	{
		//comprobamos con un bucle si el dungeon que se genera nos "sirve"
		do
		{
			generate(roomAmountMin, roomAmountMax);
			if(validate())
				return;
		}while (true);
	}

	//Funcion que checkea diferentes parametros que necesitamos para la generacion de dungeons
	boolean validate()
	{
		//variable para contar cuantos cuartos hay en el borde del mapa
		int roomsAtBorder = 0;

		Room s = null;
		Room e = null;
		//El cuarto de entrada y de salida no pueden estar demaciado cerca
		//recorremos todos los cuartos que estan puestos
		for (int yy = 0; yy < this.h; yy++)
		{
			for (int xx = 0; xx < this.w; xx++)
			{
				//Guardamos los cuartos de entrada y salida
				Room r = this.rooms[xx][yy];
				if (r == null)
					continue;
				if (r instanceof RoomStart)
					s = r;
				else if (r instanceof RoomEnd)
					e = r;	
				//contamos la cantidad de cuartos que hay en el borde del mapa
				if (r.x == 0 || r.y == 0 || r.x == this.w - 1 || r.y == this.h - 1)
					roomsAtBorder++;
			}
		}

		//Verificamos que no existan mas de cuatro cuartos en el borde del mapa
		if (roomsAtBorder > 4)
			return false;

		//calculamos la distancia por el metodo (Distancia Manhattan)
		if (abs(s.x - e.x) + abs(s.y - e.y) < 4)
			return false;

		//verificamos que el room de salida no tenga mas q un cuarto al rededor
		if (roomsAround(e) != 1)
			return false;

		return true;
	}

	//Funcion para generar cuartos de manera aleatoria (la cantidad de cuartos depende del rango del nuevo (tambien aleatorio))
	void generate(int roomAmountMin, int roomAmountMax)
	{
		this.rooms = new Room[this.w][this.h];

		//Creamos 2 arrayList 1 para los cuartos para poner y otro para el cuarto que esta puesto
		ArrayList<Room> roomsToPlace = new ArrayList<Room>();
		ArrayList<Room> roomsPlaced = new ArrayList<Room>();

		//agregamos el cuarto de entrada y el de salida al array de cuartos para poner.
		roomsToPlace.add(new RoomStart());
		roomsToPlace.add(new RoomEnd());

		//le decimos cuantos cuartos queremos que depende de un numero aleatorio dado por el min y max de cuartos
		int roomAmount = randInt(roomAmountMin, roomAmountMax - 2); //le quitamos 2 por que no se cuenta el cuarto de Start y End

		//Recorremos y agregamos los cuartos al array
		for(int r = 0; r < roomAmount; r++)
			roomsToPlace.add(new Room());

		//Checkeamos si la cantidad de cuartos es mayor a la cantidad de lugares para poner cuartos
		if(roomsToPlace.size() > this.w * this.h)
		{
			println("Too many rooms!");
			exit();
			return;
		}

		//Tomamos un cuarto al azar y lo posicionamos en un lugar al azar
		Room firstRoom = getRandomRoom(roomsToPlace);
		firstRoom.x = randInt(0, this.w);
		firstRoom.y = randInt(0, this.h);
		this.rooms[firstRoom.x][firstRoom.y] = firstRoom;
		//Sacamos el ultimo cuarto puesto de la lista para poner y lo agregamos a la lista de los cuartos puestos
		roomsToPlace.remove(firstRoom);
		roomsPlaced.add(firstRoom);

		//creamos bucle que se ejecutara hasta que no queden mas cuartos para ser puestos
		while (roomsToPlace.size() > 0)
		{
			Room roomToPlace = getRandomRoom(roomsToPlace);
			Room alreadyPlaced = getRandomRoom(roomsPlaced);
			//agregamos los cuartos uno al lado del otro
			if (placeNextTo(alreadyPlaced, roomToPlace))
			{
				roomsToPlace.remove(roomToPlace);
				roomsPlaced.add(roomToPlace);
			}
		}

		//llamamos a la funcion para crear puertas ranmdoms adicionales
		connectRandomly(0.4f);

		//recorremos todos los rooms y abrimos las puertas
		for (Room r : roomsPlaced)
			r.openDoors();
	}

	//Funcion que se fija los lugares libres que tiene al rededor alreadyPlaced y agarra uno al azar para agregar otro cuarto al lado
	boolean placeNextTo(Room alreadyPlaced, Room roomToPlace)
	{
		//Creamos lista de enteros para guardar las direcciones que tiene disponibles el cuarto
		IntList dx = new IntList();
		IntList dy = new IntList();

		//Si el x de alreadyPlaced es mayor a 0 (si tiene lugar a la izquierda disponible) y si ese casillero esta vacio
		if (alreadyPlaced.x > 0 && this.rooms[alreadyPlaced.x - 1][alreadyPlaced.y] == null)
		{
			//Los agregamos a las listas
			dx.append(-1);
			dy.append(0);
		}
		//Si el y de alreadyPlaced es mayor a 0 (si tiene lugar a la izquierda disponible) y si ese casillero esta vacio
		if (alreadyPlaced.y > 0 && this.rooms[alreadyPlaced.x][alreadyPlaced.y - 1] == null)
		{
			//Los agregamos a las listas
			dx.append(0);
			dy.append(-1);
		}

		//Si el x es menor a el ancho del dungeon -1 (tiene un lugar hacia la derecha) y si ese casillero esta vacio
		if (alreadyPlaced.x < this.w - 1 && this.rooms[alreadyPlaced.x + 1][alreadyPlaced.y] == null)
		{
			//Los agregamos a las listas
			dx.append(1);
			dy.append(0);
		}
		//Si el y es menor a el ancho del dungeon -1 (tiene un lugar hacia la derecha) y si ese casillero esta vacio
		if (alreadyPlaced.y < this.h - 1 && this.rooms[alreadyPlaced.x][alreadyPlaced.y + 1] == null)
		{
			//Los agregamos a las listas
			dx.append(0);
			dy.append(1);
		}

		//vemos si no existen lugares libres para colocar el room
		if (dx.size() == 0)
			return false;

		//si existe un lugar libre tomamos un valor al azar
		int choice = int(floor(random(0, dx.size())));
		//tomamos los offset y ubicamos el cuarto
		roomToPlace.x = alreadyPlaced.x + dx.get(choice);
		roomToPlace.y = alreadyPlaced.y + dy.get(choice);
		this.rooms[roomToPlace.x][roomToPlace.y] = roomToPlace;
		//llamamos funcion encargada de conectar los cuartos
		connectWithDoors(alreadyPlaced, roomToPlace);
		return true;
	}

	//Funcion que cuenta cuantos espacios libres tiene un cuarto
	int roomsAround(Room room)
	{
		//contamos si un cuarto tiene mas cuartos al rededor
		int count = 0;
		//Si el x de room es mayor a 0 (si tiene lugar a la izquierda disponible) y si ese casillero no esta vacio
		if (room.x > 0 && this.rooms[room.x - 1][room.y] != null)
			count++;
		//Si el y de room es mayor a 0 (si tiene lugar a la izquierda disponible) y si ese casillero no esta vacio
		if (room.y > 0 && this.rooms[room.x][room.y - 1] != null)
			count++;

		//Si el x es menor a el ancho del dungeon -1 (tiene un lugar hacia la derecha) y si ese casillero no esta vacio
		if (room.x < this.w - 1 && this.rooms[room.x + 1][room.y] != null)
			count++;
		//Si el y es menor a el ancho del dungeon -1 (tiene un lugar hacia la derecha) y si ese casillero no esta vacio
		if (room.y < this.h - 1 && this.rooms[room.x][room.y + 1] != null)
			count++;
		return count;
	}

	//Funcion encargada de conectar 2 cuartos entre si, asumiendo que en cuarto a esta abyasente del cuarto b
	void connectWithDoors(Room a, Room b)
	{
		//detectamos la relacion espacial que tienen entre los 2 cuartos
		int dx = a.x - b.x; //dx == 0 misma columna
							//dx == -1 a esta a la izquierda de b
							//dx == 1 a esta a la derecha de b
		int dy = a.y - b.y; //dx == 0 misma fila
							//dx == -1 a esta arriba de b
							//dx == 1 a esta abajo de b

		if (dx == -1)
		{
			a.doorRight = true;
			b.doorLeft = true;
		}
		else if (dx == 1)
		{
			a.doorLeft = true;
			b.doorRight = true;
		}
		else if (dy == -1)
		{
			a.doorBottom = true;
			b.doorTop = true;
		}
		else if (dy == 1)
		{
			a.doorTop = true;
			b.doorBottom = true;
		}
	}

	//Funcion encargada de conectar los cuartos en base a una chance
	void connectRandomly(float chance)
	{
		//recorremos todos los cuartos hasta la ultima fila - 1
		for (int yy = 0; yy < this.h; yy++)
		{
			for (int xx = 0; xx < this.w - 1; xx++)
			{
				//creamos variables que contengan el cuarto a y el b para verificar si tienen o no una puerta que los conecte
				Room a = this.rooms[xx][yy];
				if (a == null || a.doorRight)
					continue;

				Room b = this.rooms[xx + 1][yy];
				if (b == null)
					continue;

				//tiramos un dado si el dado da mas bajo que la chance se creara una puerta nueva en horizontal al cuarto
				if (random(1.0f) < chance)
					connectWithDoors(a, b);
			}
		}

		//recorremos todos los cuartos hasta la ultima columna - 1
		for (int yy = 0; yy < this.h - 1; yy++)
		{
			for (int xx = 0; xx < this.w; xx++)
			{
				//creamos variables que contengan el cuarto a y el b para verificar si tienen o no una puerta que los conecte
				Room a = this.rooms[xx][yy];
				if (a == null || a.doorBottom)
					continue;

				Room b = this.rooms[xx][yy + 1];
				if (b == null)
					continue;

				//tiramos un dado si el dado da mas bajo que la chance se creara una puerta nueva en vertical al cuarto
				if (random(1.0f) < chance)
					connectWithDoors(a, b);
			}
		}
	}

	//Funcion que toma al azar un cuarto entre el 0 y el maximo y lo devuelve
	Room getRandomRoom(ArrayList<Room> roomList)
	{
		int r = randInt(0, roomList.size());
		return roomList.get(r);
	}

	//Dibujamos el mapa segun los parametros que le pasamos
	void drawMap(int x, int y, int w, int h)
	{
		float roomW = w / float(this.w);
		float roomH = h / float(this.h);
		float roomBorderW = roomW * 0.1f;
		float roomBorderH = roomH * 0.1f;
		float roomDoorW = roomW * 0.3f;
		float roomDoorH = roomH * 0.3f;

		noStroke();
		fill(0, 0, 32);
		rect(x, y, w, h);

		for (int yy = 0; yy < this.h; yy++)
		{
			for (int xx = 0; xx < this.w; xx++)
			{
				Room r = this.rooms[xx][yy];
				if (r == null)
					continue;

				//dibujamos los cuartos, pintamos la entrada de verde, salida de rojo y el resto de azul
				if (r instanceof RoomStart)
					fill(0, 255, 0);
				else if (r instanceof RoomEnd)
					fill(255, 0, 0);
				else
					fill(0, 0, 255);

				//creamos parpadeo para indicar donde se encuentra parado el player
				if (r == currentRoom && frameCount % 100 < 20)
					fill(255);

				rect(x + xx * roomW + roomBorderW, y + yy * roomH + roomBorderH, 
					roomW - roomBorderW * 2, roomH - roomBorderH * 2);

				//dibujamos las puertas
				fill(0, 0, 255);
				if (r.doorLeft)
					rect(x + xx * roomW, y + yy * roomH + roomDoorH, 
						roomBorderW, roomH - roomDoorH * 2);
				if (r.doorRight)
					rect(x + xx * roomW + roomW - roomBorderW, y + yy * roomH + roomDoorH, 
						roomBorderW, roomH - roomDoorH * 2);
				if (r.doorTop)
					rect(x + xx * roomW + roomDoorW, y + yy * roomH, 
						roomW - roomDoorW * 2, roomBorderH);
				if (r.doorBottom)
					rect(x + xx * roomW + roomDoorW, y + yy * roomH + roomH - roomBorderH, 
						roomW - roomDoorW * 2, roomBorderH);
			}
		}
	}
};

