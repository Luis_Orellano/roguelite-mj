//Clase encargada de realizar los cortes de las imagenes y prepararlas para animarlas
class SpriteSheet
{
    PImage[] frames;

    SpriteSheet(String ssheetPath, int frameW, int frameH, int paddingX, int paddingY)
    {
        //cargamos el spritesheet y calculamos cuanto miden en alto y ancho
        PImage ssheet = loadImage(ssheetPath);
        int framesInX = ssheet.width / (frameW + paddingX);
        int framesInY = ssheet.height / (frameH + paddingY);

        //Armamos el array de frames y lo recorremos
        this.frames = new PImage[framesInX * framesInY];
        for (int y = 0; y < framesInY; y++)
        {
            for (int x = 0; x < framesInX; x++)
            {
                //creamos una imagen y le copiamos el sheet original
                PImage frame = createImage(frameW, frameH, ARGB);
                frame.copy(ssheet, x * (frameW + paddingX), y * (frameH + paddingY), frameW, frameH, 0, 0, frameW, frameH);
                //guardamos los frames en un array unidimensional como si fuesen pixeles
                this.frames[x + y * framesInX] = frame;
            }
        }
    }
};